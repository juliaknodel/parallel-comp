#include "images_processing.h"


int ** allocate_pgm_memory(int row, int col) {
	int ** arr;

	arr = (int **)malloc(sizeof(int *) * row);

	if (arr == NULL) {
		printf("Memory allocation error");
		return NULL;
	}

	for (int i = 0; i < row; i++) {
		arr[i] = (int *)malloc(sizeof(int) * col);
		if (arr[i] == NULL) {
			printf("Memory allocation error");
			return NULL;
		}
	}

	return arr;
}

void free_pgm_memory(int **arr, int row) {

	for (int i = 0; i < row; i++) {
		free(arr[i]);
	}

	free(arr);
}


PGM_DATA* read_PGM(const char* file_name, PGM_DATA* data) {
	FILE* pgm_file;
	char version[3];

	pgm_file = fopen(file_name, "rb");

	if (pgm_file == NULL) {
		printf("Can't open file");
	}

	fgets(version, sizeof(version), pgm_file);

	fscanf(pgm_file, "%d", &(data->col));
	fscanf(pgm_file, "%d", &(data->row));
	fscanf(pgm_file, "%d", &(data->max_gray));

	data->intensity = allocate_pgm_memory(data->row, data->col);
	data->blur = allocate_pgm_memory(data->row, data->col);

	for (int i = 0; i < data->row; i++) {
		for (int j = 0; j < data->col; j++) {
			fscanf(pgm_file, "%d", &((data->intensity)[i][j]));
		}
	}

	fclose(pgm_file);
	return data;

}


int get_max(PGM_DATA* data){

    int max_val = 0;

    for (int i = 1; i< data->row - 1; i++){

        for (int j = 1; j < data->col - 1; j++){

            if (data->intensity[i][j] != INFINITY && data->intensity[i][j] > max_val)
            {
                max_val = data->intensity[i][j];
            }

        }
    }

    return max_val;
}


void write_result(PGM_DATA* data) {
    int temp = 0;
	int width = data->col, height = data->row;

	FILE* pgmimg;
	pgmimg = fopen("result.pgm", "wb");

	// Writing Magic Number to the File
	fprintf(pgmimg, "P2\n");

	// Writing Width and Height
	fprintf(pgmimg, "%d %d\n", width, height);

	// Writing the maximum gray value
	int max_grey = 255;

	fprintf(pgmimg, "255\n");

	int max_val = get_max(data);

    float coeff = (float)max_grey / (float)max_val;

	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {

            temp = data->intensity[i][j] * coeff;

			// Writing the gray values in the 2D array to the file
			fprintf(pgmimg, "%d ", temp);
		}
		fprintf(pgmimg, "\n");
	}

	fclose(pgmimg);
}

