#include "images_processing.h"
#include <omp.h>
#include<time.h>


#ifdef INFINITY
/* INFINITY is supported */
#endif

double delta_T = 0.1;
double k = 10.0;
int max_iter_num = 50;


int get_grad(int side_val, int cur_val) {

    return side_val - cur_val;
}

double g(int x) {
    return 1 / ( 1 + (x / k)*(x / k) );
}

void calc_down(PGM_DATA* data, int row, int col) {

	// i - rows, Y; j - cols, X


    // number of rows in parallel block
    int block_size = row / omp_get_num_threads();

    // idx of first and last rows in block
    int start_i = block_size * omp_get_thread_num();
    int end_i = start_i + block_size - 1;


    // last process may have extra data
    if (omp_get_thread_num() + 1 == omp_get_num_threads()){
        end_i = row - 1;
    }

    int value_N = 0;
    int value_S = 0;
    int value_W = 0;
    int value_E = 0;
    int grad_N = 0;
    int grad_S = 0;
    int grad_W = 0;
    int grad_E = 0;


    for (int i = start_i; i <= end_i; i++) {
        for (int j = 0; j < col; j++) {

            // current intensity
            int intensity = data->intensity[i][j];

            // here we need 4 more values - NEWS
            if (j == 0){
                value_E = 0;
            }
            else value_E = data->intensity[i][j - 1];

            if (j == col - 1){
                value_W = 0;
            }
            else value_W = data->intensity[i][j + 1];


            // reading from prev thread area
            if (start_i != 0 && i == start_i){

                #pragma omp critical (border_read)
                {
                    value_N = data->intensity[i - 1][j];
                }
            }
            else {

                if (i == 0){
                    value_N = 0;
                }
                else value_N = data->intensity[i - 1][j];

            }

            // reading from next thread area
            if (end_i != row - 1 && i == end_i){

                #pragma omp critical (border_read)
                {
                    value_S = data->intensity[i + 1][j];
                }
            }
            else {

                if (i == row - 1){
                    value_S = 0;
                }
                else value_S = data->intensity[i + 1][j];
            }


            grad_N = get_grad(value_N, intensity);
            grad_S = get_grad(value_S, intensity);
            grad_W = get_grad(value_W, intensity);
            grad_E = get_grad(value_E, intensity);

            data->blur[i][j] = intensity + delta_T * (g(abs(grad_N)) * grad_N + g(abs(grad_S)) * grad_S + g(abs(grad_W)) * grad_W + g(abs(grad_E)) * grad_E);

        }
    }
}




void upd_intense_arr(PGM_DATA* data) {
    int ** temp = data->blur;
    data->blur = data->intensity;
    data->intensity = temp;
}

void calc_blur(PGM_DATA* data) {


    for (int i = 0; i < max_iter_num; i++){


        #pragma omp parallel num_threads(4)
        {
            int row, col;
            #pragma omp critical (rowcol)
            {
                row = data -> row;
                col = data -> col;
            }

            calc_down(data, row, col);
        }

        upd_intense_arr(data);
    }
}

void print_solution(PGM_DATA* data) {

	for (int i = 0; i < data->row; i++) {
		for (int j = 0; j < data->col; j++) {
			printf("%d ", data->intensity[i][j]);
		}
		printf("\n");
	}

	printf("\n\n\n");
}



void run(const char* filename) {

	PGM_DATA* data = (PGM_DATA*)malloc(sizeof(PGM_DATA));

	data = read_PGM(filename, data);

	calc_blur(data);

	write_result(data);

	free_pgm_memory(data->intensity, data->row);
	free_pgm_memory(data->blur, data->row);

}


int main() {

    FILE *fpt;

    const char* filename = "data/lena.pgm";
    const char* res_filename = "omp_res_1.csv";


    // run(filename);


    int n_threads = 4;
    int input_img_type = 0; // 0 - lena, 1 - bio
    int iter = 1;

    if (fpt = fopen(res_filename, "r"))
    {
        fclose(fpt);
        fpt = fopen(res_filename, "a");
    }
    else {
        fpt = fopen(res_filename, "a");
        fprintf(fpt,"threads; iters; time; img_type\n");
    }

    double sum_time = 0;


    for (int i = 0; i < iter; i++){
        clock_t begin = clock();

        run(filename);

        clock_t end = clock();

        double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

        fprintf(fpt,"%d;%d;%f;%d\n", n_threads, max_iter_num, time_spent, input_img_type);

        sum_time += time_spent;

    }

    fclose(fpt);


    return 0;
}



