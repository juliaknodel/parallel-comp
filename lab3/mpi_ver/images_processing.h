#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


typedef struct PGM_DATA {
	int row;
	int col;
	int max_gray;
	int **intensity;
	int **blur;
} PGM_DATA;

int ** allocate_pgm_memory(int row, int col);

void free_pgm_memory(int **arr, int row);

PGM_DATA* read_PGM(const char* file_name, PGM_DATA* data);

int get_max(PGM_DATA* data);

void write_result(PGM_DATA* data);
