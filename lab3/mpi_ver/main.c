#include "images_processing.h"
#include<mpi.h>
#include<time.h>

#ifdef INFINITY
/* INFINITY is supported */
#endif

#define ROW_NUM 0
#define COL_NUM 1
#define PGM_DAT 2

#define SEND_PREV 5
#define FROM_NEXT 5
#define SEND_NEXT 6
#define FROM_PREV 6

#define FINAL_VER 7


MPI_Status status;

double delta_T = 0.1;
double k = 10.0;
int max_iter_num = 100;


int get_grad(int side_val, int cur_val) {

    return side_val - cur_val;
}

double g(int x) {
    return 1 / ( 1 + (x / k)*(x / k) );
}


void calc_down(PGM_DATA* data, int myid, int numprocs, int start_i, int end_i) {

	// i - rows, Y; j - cols, X
    int value_N = 0;
    int value_S = 0;
    int value_W = 0;
    int value_E = 0;
    int grad_N = 0;
    int grad_S = 0;
    int grad_W = 0;
    int grad_E = 0;

    int row = data -> row;
    int col = data->col;

	for (int i = start_i; i <= end_i; i++) {
		for (int j = 0; j < col; j++) {

			int intensity = data->intensity[i][j];

            // here we need 4 more values - NEWS
            if (j == 0){
                value_E = 0;
            }
            else value_E = data->intensity[i][j - 1];

            if (j == col - 1){
                value_W = 0;
            }
            else value_W = data->intensity[i][j + 1];

            if (i == 0){
                value_N = 0;
            }
            else value_N = data->intensity[i - 1][j];

            if (i == row - 1){
                value_S = 0;
            }
            else value_S = data->intensity[i + 1][j];


            grad_N = get_grad(value_N, intensity);
            grad_S = get_grad(value_S, intensity);
            grad_W = get_grad(value_W, intensity);
            grad_E = get_grad(value_E, intensity);

            data->blur[i][j] = intensity + delta_T * (g(abs(grad_N)) * grad_N + g(abs(grad_S)) * grad_S + g(abs(grad_W)) * grad_W + g(abs(grad_E)) * grad_E);

		}
	}

	return;
}


void print_solution(PGM_DATA* data) {

	for (int i = 0; i < data->row; i++) {
		for (int j = 0; j < data->col; j++) {
			printf("%d ", data->intensity[i][j]);
		}
		printf("\n");
	}

	printf("\n\n\n");
}


void upd_intense_arr(PGM_DATA* data, int start_i, int end_i) {

    int* temp;

    for (int i = start_i; i <= end_i; i++){
        temp = data->blur[i];
        data->blur[i] = data->intensity[i];
        data->intensity[i] = temp;
    }
}


void receive_data_from_other_processes(PGM_DATA* data, int numprocs){

    int block_size = data->row / numprocs;

    for (int proc_num = 1; proc_num < numprocs - 1; proc_num++){

        MPI_Recv( &(data->intensity[block_size * proc_num][0]), data->col * block_size, MPI_INT, proc_num, FINAL_VER, MPI_COMM_WORLD, &status );
    }

    // last process may have more data
    if (numprocs != 1){
        int last_proc_start_i = block_size * (numprocs - 1);
        int last_proc_end_i = data->row - 1;

        MPI_Recv( &(data->intensity[last_proc_start_i][0]), data->col * (last_proc_end_i - last_proc_start_i + 1), MPI_INT, numprocs - 1, FINAL_VER, MPI_COMM_WORLD, &status );
    }
}

void send_data_to_main_process(PGM_DATA* data, int numprocs, int myid){

    // number of rows in parallel block
    int block_size = data->row / numprocs;

    // idx of first and last rows in block
    int start_i = block_size * myid;
    int end_i = start_i + block_size - 1;

    // last process may have extra data
    if (myid + 1 == numprocs){
        end_i = data->row - 1;
    }

    MPI_Send( &(data->intensity[start_i][0]), data->col * (end_i - start_i + 1), MPI_INT, 0, FINAL_VER, MPI_COMM_WORLD );
}


void calc_blur(PGM_DATA* data, int myid, int numprocs) {

    // number of rows in parallel block
    int block_size = data->row / numprocs;

    // idx of first and last rows in block
    int start_i = block_size * myid;
    int end_i = start_i + block_size - 1;

    // last process may have extra data
    if (myid + 1 == numprocs){
        end_i = data->row - 1;
    }

    int cur_iter = 0;


	while (cur_iter < max_iter_num) {

		calc_down(data, myid, numprocs, start_i, end_i);

		// move blurred data to intensity array
		upd_intense_arr(data, start_i, end_i);


        // if odd -> first send, then receive
        if (myid % 2){

            // 0 process doesnt have prev process
            if (myid != 0){
                MPI_Send( &(data->intensity[start_i][0]), data->col, MPI_INT, myid - 1, SEND_PREV, MPI_COMM_WORLD );
            }
            // last process doesnt have next process
            if (myid + 1 != numprocs){
                MPI_Send( &(data->intensity[end_i][0]), data->col, MPI_INT, myid + 1, SEND_NEXT, MPI_COMM_WORLD );
            }

            // all except last process gets an update on next row outside their block
            if (myid + 1 != numprocs){
                MPI_Recv( &(data->intensity[end_i + 1][0]), data->col, MPI_INT, myid + 1, FROM_NEXT, MPI_COMM_WORLD, &status );
            }
            // all except 0 process gets an update on prev row outside their block
            if (myid != 0){
                MPI_Recv( &(data->intensity[start_i - 1][0]), data->col, MPI_INT, myid - 1, FROM_PREV, MPI_COMM_WORLD, &status );
            }
        }
        else {
            // all except last process gets an update on next row outside their block
            if (myid + 1 != numprocs){
                MPI_Recv( &(data->intensity[end_i + 1][0]), data->col, MPI_INT, myid + 1, FROM_NEXT, MPI_COMM_WORLD, &status );
            }
            // all except 0 process gets an update on prev row outside their block
            if (myid != 0){
                MPI_Recv( &(data->intensity[start_i - 1][0]), data->col, MPI_INT, myid - 1, FROM_PREV, MPI_COMM_WORLD, &status );
            }

            // 0 process doesnt have prev process
            if (myid != 0){
                MPI_Send( &(data->intensity[start_i][0]), data->col, MPI_INT, myid - 1, SEND_PREV, MPI_COMM_WORLD );
            }
            // last process doesnt have next process
            if (myid + 1 != numprocs){
                MPI_Send( &(data->intensity[end_i][0]), data->col, MPI_INT, myid + 1, SEND_NEXT, MPI_COMM_WORLD );
            }
        }

		cur_iter += 1;

	}
}


void run(const char* filename, int myid, int numprocs) {
	srand(time(NULL));

	PGM_DATA* data = (PGM_DATA*)malloc(sizeof(PGM_DATA));

    // only 0 process works with files
    if (myid == 0){

        data = read_PGM(filename, data);

        // sends row, col nums
        for (int rec_proc_num = 1; rec_proc_num < numprocs; rec_proc_num++){
            MPI_Send( &(data->col), 1, MPI_INT, rec_proc_num, COL_NUM, MPI_COMM_WORLD );
            MPI_Send( &(data->row), 1, MPI_INT, rec_proc_num, ROW_NUM, MPI_COMM_WORLD );
        }

        // sends intensity array from file
        for (int rec_proc_num = 1; rec_proc_num < numprocs; rec_proc_num++){
            MPI_Send( &(data->intensity[0][0]), data->row * data->col, MPI_INT, rec_proc_num, PGM_DAT, MPI_COMM_WORLD);
        }
    }
    else {
        // gets rows cols num from 0 process
        MPI_Recv( &(data->col), 1, MPI_INT, 0, COL_NUM, MPI_COMM_WORLD, &status );
        MPI_Recv( &(data->row), 1, MPI_INT, 0, ROW_NUM, MPI_COMM_WORLD, &status );

        data->intensity = allocate_pgm_memory(data->row, data->col);
        data->blur = allocate_pgm_memory(data->row, data->col);

        // gets intensity array
        MPI_Recv( &(data->intensity[0][0]), data->row * data->col, MPI_INT, 0, PGM_DAT, MPI_COMM_WORLD, &status );
    }

	calc_blur(data, myid, numprocs);

	if (myid == 0){
        receive_data_from_other_processes(data, numprocs);
        write_result(data);
	}
	else send_data_to_main_process(data, numprocs, myid);

	free_pgm_memory(data->intensity, data->row);
	free_pgm_memory(data->blur, data->row);

	return;
}

int main(int argc, char* argv[])
{
    int myid, numprocs;

    MPI_Init(&argc, &argv);        // starts MPI

    FILE *fpt;
    // const char* filename = "data/lena.pgm";
    const char* filename = "data/cells.pgm";

    const char* res_filename = "mpi_res_1.csv";
    int input_img_type = 0;  // 0 lena, 1 - bio
    int iters_num = 0;


    MPI_Comm_rank(MPI_COMM_WORLD, &myid);  // get current process id
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);      // get number of processes

    clock_t begin = clock();

    run(filename, myid, numprocs);

    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;


    if (myid == 0){

        printf("Process number %d time: %f\n", myid, time_spent);

        if (fpt = fopen(res_filename, "r"))
        {
            fclose(fpt);
            fpt = fopen(res_filename, "a");
            fprintf(fpt,"%d;%d;%f;%d\n", numprocs, iters_num, time_spent, input_img_type);

        }
        else {
            fpt = fopen(res_filename, "a");
            fprintf(fpt,"threads; iters; time; img_type\n");
            fprintf(fpt,"%d;%d;%f;%d\n", numprocs, max_iter_num, time_spent, input_img_type);
        }

        fclose(fpt);

    }


    MPI_Finalize();

    return 0;
}


