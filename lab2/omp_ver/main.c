// C program to read a BMP Image and
// write the same into a PGM Image file
#include "images_processing.h"
#include <omp.h>
#include<time.h>


#ifdef INFINITY
/* INFINITY is supported */
#endif

const int MAX_SRC_NUM = 20;
const int WIN_SIZE = 3;
const int SRC_BRIGHT = 200;
int flag = 1;


float ** get_init_distance(PGM_DATA* data) {

	data->distance = allocate_dist_memory(data->row, data->col);

	int src_number = 0;

	for (int i = 0; i < data->row; i++) {
		for (int j = 0; j < data->col; j++) {

			data->distance[i][j] = INFINITY;
            // for non-random bright-based sources
            if (src_number < MAX_SRC_NUM && data->intensity[i][j] > SRC_BRIGHT){
                data->distance[i][j] = 0;
                // ��������� �����, ��� � ��� ����� ���� �� 1 ��������
                src_number += 1;
            }
		}
	}

	// ���� �� ������� ���������� ����� ���������
	if (src_number == 0){
        data->distance[0][0] = 0;
	}

	// for random src_num sources
    /*
	for (int i = 0; i < SRC_NUM; i++) {
		int rand_x = rand() % data->col;
		int rand_y = rand() % data->row;

		// source
		data->distance[rand_y][rand_x] = 0;
	}
	*/



	return data->distance;
}


float get_min(float* values, int num_comparison) {
	float min_val = values[0];

	for (int i = 1; i < num_comparison; i++) {
		if (values[i] < min_val) {
			min_val = values[i];
		}
	}

	return min_val;
}


void calc_down(PGM_DATA* data, int* solution_changed, int row, int col) {

	// i - rows, Y; j - cols, X
	// distance[Y][X]
	int num_comparison = 5;
    float a = (float) sqrt(2);
	float b = 1;

	int priv_solution_changed  = 0;

    float* values = (float*) malloc(sizeof(float) * num_comparison);

        // ���-�� ����� � ����� ������������ �����
        int block_size = row / omp_get_num_threads();

        // ������� ������ � ��������� ����� ������������� �����
        int start_i = block_size * omp_get_thread_num();
        int end_i = start_i + block_size - 1;

        // ���������� ������
        start_i -= 1;
        end_i += 1;

        // ������ ���� ��� ���������� ������ � - 1
        if (omp_get_thread_num() == 0){
                start_i = 0;
        }

        // ��� ���������� ����� ����� ������� ���������� ������, ���� ��� ��������
        if (omp_get_thread_num() + 1 == omp_get_num_threads()){
                end_i = row - 1;
        }



        // ���� �� ��� �������, � ������� ����� ����������� ������ ������� �������
        for (int i = start_i + 1; i < end_i; i++) {
            for (int j = 1; j < col - 1; j++) {

                float intensity = (float)(data->intensity[i][j]);


                values[0] = data->distance[i][j - 1] + a * intensity;
                values[4] = data->distance[i][j];


                // ������ ������, ���� ����� ������ ���������� (�� ������) thread
                if (start_i != 0 && i - 1 == start_i){
                    #pragma omp critical (border_read)
                    {
                        values[1] = data->distance[i - 1][j] + a * intensity;
                        values[2] = data->distance[i - 1][j - 1] + b * intensity;
                        values[3] = data->distance[i - 1][j + 1] + b * intensity;
                    }
                }
                else{
                    values[1] = data->distance[i - 1][j] + a * intensity;
                    values[2] = data->distance[i - 1][j - 1] + b * intensity;
                    values[3] = data->distance[i - 1][j + 1] + b * intensity;
                }

                float new_dist = get_min(values, num_comparison);

                // ��� ������ � ��� �������, � ������� ����� ������ ������ ������� thread
                if (new_dist != data->distance[i][j]) {

                    // ��� ������ 1 ����� ������ ���� ���-�� ��� ������ ������
                    if (i == end_i - 1){
                        #pragma omp critical (border_read)
                        {
                            data->distance[i][j] = new_dist;
                        }
                    }
                    else data->distance[i][j] = new_dist;

                    priv_solution_changed  = 1;
                }
            }
        }
        // main cycle end

        if (priv_solution_changed){
            #pragma omp critical (solution_changed)
            {
                *solution_changed = 1;
            }
        }

        free(values);
}


void calc_up(PGM_DATA* data, int* solution_changed, int row, int col) {

	// i - rows, Y; j - cols, X
	// distance[Y][X]
	int num_comparison = 5;
    float a = (float) sqrt(2);
	float b = 1;

    int priv_solution_changed = 0;

    float* values = (float*) malloc(sizeof(float) * num_comparison);

    // ���-�� ����� � ����� ������������ �����
    int block_size = row / omp_get_num_threads();

    // ������� ������ � ��������� ����� ������������� �����
    int start_i = block_size * omp_get_thread_num();
    int end_i = start_i + block_size - 1;

    // ���������� ������
    start_i -= 1;
    end_i += 1;

    // ������ ���� ��� ���������� ������ � - 1
    if (omp_get_thread_num() == 0){
            start_i = 0;
    }

    // ��� ���������� ����� ����� ������� ���������� ������, ���� ��� ��������
    if (omp_get_thread_num() + 1 == omp_get_num_threads()){
            end_i = row - 1;
    }


        // ���� �� ��� �������, � ������� ����� ����������� ������ ������� �������
        for (int i = end_i - 1; i > start_i; i--) {
            for (int j = col - 2; j >= 1; j--) {

                float intensity = (float)(data->intensity[i][j]);


                // how it should be
                values[0] = data->distance[i][j + 1] + a * intensity;
                values[4] = data->distance[i][j];

                if (end_i != row - 1 && i + 1 == end_i)
                {
                    #pragma omp critical (border_read)
                    {
                        values[1] = data->distance[i + 1][j] + a * intensity;
                        values[2] = data->distance[i + 1][j + 1] + b * intensity;
                        values[3] = data->distance[i + 1][j - 1] + b * intensity;

                    }
                }
                else
                {
                    values[1] = data->distance[i + 1][j] + a * intensity;
                    values[2] = data->distance[i + 1][j + 1] + b * intensity;
                    values[3] = data->distance[i + 1][j - 1] + b * intensity;
                }

                float new_dist = get_min(values, num_comparison);

                // ��� ������ � ��� �������, � ������� ����� ������������ ������� thread
                if (new_dist != data->distance[i][j]) {

                    // ��� ������ 1 ����� ������ ���� ���-�� ��� ������ ������
                    if (i == start_i + 1){
                        #pragma omp critical (border_read)
                        {
                            data->distance[i][j] = new_dist;
                        }
                    }
                    else data->distance[i][j] = new_dist;

                    priv_solution_changed  = 1;
                }
            }
        }
        // main cycle end

        if (priv_solution_changed){
            #pragma omp critical (solution_changed)
            {
                *solution_changed = 1;
            }
        }

        free(values);
}


void print_solution(PGM_DATA* data) {

	for (int i = 0; i < data->row; i++) {
		for (int j = 0; j < data->col; j++) {
			printf("%f ", data->distance[i][j]);
		}
		printf("\n");
	}

	printf("\n\n\n");
}

float** calc_dist(PGM_DATA* data, int* iters_num) {

	int solution_changed = 1;
	int max_iter = 1000;
	int cur_iter = 0;

	while (cur_iter < max_iter) {
        #pragma omp parallel num_threads(4)
        {
            int row, col;
            #pragma omp critical (rowcol)
            {
                row = data -> row;
                col = data -> col;
            }

            calc_down(data, &solution_changed, row, col);
            calc_up(data, &solution_changed, row, col);
        }

		if (!solution_changed) {
			printf("\nSolution did not changed on %d iteration", cur_iter);
			break;
		}

		solution_changed = 0;
		cur_iter += 1;
	}

    *iters_num = cur_iter;

	return data->distance;
}


int run(const char* filename) {
	srand(time(NULL));

	PGM_DATA* data = (PGM_DATA*)malloc(sizeof(PGM_DATA));

	data = read_PGM(filename, data);
	data->distance = get_init_distance(data);

    int iters_num = 0;

	data->distance = calc_dist(data, &iters_num);

	write_result(data);

	free_pgm_memory(data->intensity, data->row);
	free_dist_memory(data->distance, data->row);

	return iters_num;
}


int main() {

    int img = 0;

    if (!img){  // ������ ��� �������� ����, ���� ������ ����������� ��� ������������� ��������

    FILE *fpt, *file;

    const char* filename = "data/pgmimg_1.pgm";
    const char* res_filename = "omp_res.csv";
    int n_threads = 4;
    int input_img_type = 0;  // 0 500x500; 1 - 50x5000; format - colsxrows

    if (fpt = fopen(res_filename, "r"))
    {
        fclose(fpt);
        fpt = fopen(res_filename, "a");
    }
    else {
        fpt = fopen(res_filename, "a");
        fprintf(fpt,"threads; iters; time; img_type\n");
    }

    double sum_time = 0;
    int iter = 1;
    int iters_num = 0;

    for (int i = 0; i<iter; i++){
        clock_t begin = clock();

        iters_num = run(filename);

        clock_t end = clock();

        double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

        fprintf(fpt,"%d;%d;%f;%d\n", n_threads, iters_num, time_spent, input_img_type);

        sum_time += time_spent;

    }

    fclose(fpt);

    printf("Execution time: %f\n", sum_time / iter);

    }
    else generate_grey_arr();

    return 0;
}



