#include "images_processing.h"


void create_pgm()
{
	int i, j, temp = 0;
	int width = 13, height = 13;

	// Suppose the 2D Array to be converted to Image is as given below
	int image[13][13] = {
	  { 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15 },
	  { 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31},
	  { 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47},
	  { 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63},
	  { 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79, 79},
	  { 95, 95, 95, 95, 95, 95, 95, 95, 95, 95, 95, 95, 95 },
	  { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111},
	  { 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127},
	  { 143, 143, 143, 143, 143, 143, 143, 143, 143, 143, 143, 143, 143},
	  { 159, 159, 159, 159, 159, 159, 159, 159, 159, 159, 159, 159, 159},
	  { 175, 175, 175, 175, 175, 175, 175, 175, 175, 175, 175, 175, 175},
	  { 191, 191, 191, 191, 191, 191, 191, 191, 191, 191, 191, 191, 191},
	  { 207, 207, 207, 207, 207, 207, 207, 207, 207, 207, 207, 207, 207}
	};

	FILE* pgmimg;
	pgmimg = fopen("pgmimg.pgm", "wb");

	// Writing Magic Number to the File
	fprintf(pgmimg, "P2\n");

	// Writing Width and Height
	fprintf(pgmimg, "%d %d\n", width, height);

	// Writing the maximum gray value
	fprintf(pgmimg, "255\n");
	int count = 0;
	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			temp = image[i][j];

			// Writing the gray values in the 2D array to the file
			fprintf(pgmimg, "%d ", temp);
		}
		fprintf(pgmimg, "\n");
	}
	fclose(pgmimg);
}


int ** allocate_pgm_memory(int row, int col) {
	int ** arr;

	arr = (int **)malloc(sizeof(int *) * row);

	if (arr == NULL) {
		printf("Memory allocation error");
		return NULL;
	}

	for (int i = 0; i < row; i++) {
		arr[i] = (int *)malloc(sizeof(int) * col);
		if (arr[i] == NULL) {
			// TODO free memory???
			printf("Memory allocation error");
			return NULL;
		}
	}

	return arr;
}

void free_pgm_memory(int **arr, int row) {

	for (int i = 0; i < row; i++) {
		free(arr[i]);
	}

	free(arr);
}

float ** allocate_dist_memory(int row, int col) {
	float ** arr;

	arr = (float **)malloc(sizeof(float *) * row);

	if (arr == NULL) {
		printf("Memory allocation error");
		return NULL;
	}

	for (int i = 0; i < row; i++) {
		arr[i] = (float *)malloc(sizeof(float) * col);
		if (arr[i] == NULL) {
			// TODO free memory???
			printf("Memory allocation error");
			return NULL;
		}
	}

	return arr;
}

void free_dist_memory(float **arr, int row) {

	for (int i = 0; i < row; i++) {
		free(arr[i]);
	}

	free(arr);
}


PGM_DATA* read_PGM(const char* file_name, PGM_DATA* data) {
	FILE* pgm_file;
	char version[3];

	pgm_file = fopen(file_name, "rb");

	if (pgm_file == NULL) {
		printf("Can't open file");
	}

	fgets(version, sizeof(version), pgm_file);
	// printf("Version: %s", version);

	fscanf(pgm_file, "%d", &(data->col));
	fscanf(pgm_file, "%d", &(data->row));
	fscanf(pgm_file, "%d", &(data->max_gray));

	data->intensity = allocate_pgm_memory(data->row, data->col);

	for (int i = 0; i < data->row; i++) {
		for (int j = 0; j < data->col; j++) {
			fscanf(pgm_file, "%d", &((data->intensity)[i][j]));

			// printf("%d\n", (data->intensity)[i][j]);
		}
	}

	fclose(pgm_file);
	return data;

}

// ��������� ������
void generate_grey_arr() {
	int i, j, temp = 0;
	int width = 50, height = 5000;

	FILE* pgmimg;
	pgmimg = fopen("pgmimg_50_5000.pgm", "wb");

	// Writing Magic Number to the File
	fprintf(pgmimg, "P2\n");

	// Writing Width and Height
	fprintf(pgmimg, "%d %d\n", width, height);

	printf("SQRT: %f\n\n\n", (float)sqrt(2));

	// Writing the maximum gray value
	fprintf(pgmimg, "255\n");

	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			temp = rand() % 256;

			// Writing the gray values in the 2D array to the file
			fprintf(pgmimg, "%d ", temp);
		}
		fprintf(pgmimg, "\n");
	}

	fclose(pgmimg);
}

int get_max(PGM_DATA* data){

    int max_val = 0;

    for (int i = 1; i< data->row - 1; i++){

        for (int j = 1; j < data->col - 1; j++){

            if (data->distance[i][j] != INFINITY && data->distance[i][j] > max_val)
            {
                max_val = data->distance[i][j];
            }

        }
    }

    return max_val;
}


void write_result(PGM_DATA* data) {
    int temp = 0;
	int width = data->col, height = data->row;

	FILE* pgmimg;
	pgmimg = fopen("result.pgm", "wb");

	// Writing Magic Number to the File
	fprintf(pgmimg, "P2\n");

	// Writing Width and Height
	fprintf(pgmimg, "%d %d\n", width, height);

	// Writing the maximum gray value
	int max_grey = 255;

	fprintf(pgmimg, "255\n");

	int max_val = get_max(data);

    float coeff = (float)max_grey / (float)max_val;

	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {

            if (i == 0 || i == height - 1 || j == 0 || j == width - 1){
                temp = 0;
            }
			else {
                temp = data->distance[i][j] * coeff;
			}

			// Writing the gray values in the 2D array to the file
			fprintf(pgmimg, "%d ", temp);
		}
		fprintf(pgmimg, "\n");
	}

	fclose(pgmimg);
}

