#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


typedef struct PGM_DATA {
	int row;
	int col;
	int max_gray;
	int **intensity;
	float **distance;
} PGM_DATA;


void create_pgm();


int ** allocate_pgm_memory(int row, int col);

void free_pgm_memory(int **arr, int row);

float ** allocate_dist_memory(int row, int col);

void free_dist_memory(float **arr, int row);

PGM_DATA* read_PGM(const char* file_name, PGM_DATA* data);

void generate_grey_arr();

int get_max(PGM_DATA* data);

void write_result(PGM_DATA* data);
