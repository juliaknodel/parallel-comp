// C program to read a BMP Image and
// write the same into a PGM Image file
#include "images_processing.h"
#include<mpi.h>
#include<time.h>

#ifdef INFINITY
/* INFINITY is supported */
#endif

#define ROW_NUM 0
#define COL_NUM 1
#define PGM_DAT 2
#define DIST_DAT 3
#define ITER_STATUS 4
#define SEND_PREV 5
#define FROM_NEXT 5
#define SEND_NEXT 6
#define FROM_PREV 6
#define FINAL_VER 7

const int MAX_SRC_NUM = 20;
const int WIN_SIZE = 3;
const int SRC_BRIGHT = 200;
MPI_Status status;

// int myid, numprocs, namelen;

float ** get_init_distance(PGM_DATA* data) {

	data->distance = allocate_dist_memory(data->row, data->col);

	int src_number = 0;

	for (int i = 0; i < data->row; i++) {
		for (int j = 0; j < data->col; j++) {

			data->distance[i][j] = INFINITY;
            // for non-random bright-based sources
            if (src_number < MAX_SRC_NUM && data->intensity[i][j] > SRC_BRIGHT){
                data->distance[i][j] = 0;
                // ��������� �����, ��� � ��� ����� ���� �� 1 ��������
                src_number += 1;
            }
		}
	}

	// ���� �� ������� ���������� ����� ���������
	if (src_number == 0){
        data->distance[0][0] = 0;
	}

	return data->distance;
}


float get_min(float* values, int num_comparison) {
	float min_val = values[0];

	for (int i = 1; i < num_comparison; i++) {
		if (values[i] < min_val) {
			min_val = values[i];
		}
	}

	return min_val;
}


float ** calc_down(PGM_DATA* data, int* solution_changed, int myid, int numprocs) {

	// i - rows, Y; j - cols, X
	// distance[Y][X]
	int num_comparison = 5;
	float* values = (float*) malloc(sizeof(float) * num_comparison);

	float a = (float) sqrt(2);
	float b = 1;

    // ���-�� ����� � ����� ������������ �����
    int block_size = data->row / numprocs;

    // ������� ������ � ��������� ����� ������������� �����
    int start_i = block_size * myid;
    int end_i = start_i + block_size - 1;

    // ���������� ������
    start_i -= 1;
    end_i += 1;

    // ������ ���� ��� ���������� ������ � - 1
    if (myid == 0){
        start_i = 0;
    }

    // ��� ���������� ����� ����� ������� ���������� ������, ���� ��� ��������
    if (myid + 1 == numprocs){
        end_i = data->row - 1;
    }

    // ������ �������� �� �������������� ������, ������� ���� �� ���� ��������, �� �� �� ���� �������
	for (int i = start_i + 1; i < end_i; i++) {
		for (int j = 1; j < data->col - 1; j++) {

			float intensity = (float)(data->intensity[i][j]);

			values[0] = data->distance[i][j - 1] + a * intensity;
			values[1] = data->distance[i - 1][j] + a * intensity;
			values[2] = data->distance[i - 1][j - 1] + b * intensity;
			values[3] = data->distance[i - 1][j + 1] + b * intensity;
			values[4] = data->distance[i][j];

			float new_dist = get_min(values, num_comparison);


			if (new_dist != data->distance[i][j]) {

				data->distance[i][j] = new_dist;

				*solution_changed = 1;
			}
		}
	}

	free(values);

	return data->distance;
}


float ** calc_up(PGM_DATA* data, int* solution_changed, int myid, int numprocs) {

	// i - rows, Y; j - cols, X
	// distance[Y][X]

	int num_comparison = 5;
	float* values = (float*)malloc(sizeof(float) * num_comparison);

	float a = (float) sqrt(2);
	float b = 1;

    // ���-�� ����� � ����� ������������ �����
    int block_size = data->row / numprocs;

    // ������� ������ � ��������� ����� ������������� �����
    int start_i = block_size * myid;
    int end_i = start_i + block_size - 1;

    // ���������� ������
    start_i -= 1;
    end_i += 1;

    // ������ ���� ��� ���������� ������ � - 1
    if (myid == 0){
        start_i = 0;
    }

    // ��� ���������� ����� ����� ������� ���������� ������, ���� ��� ��������
    if (myid + 1 == numprocs){
        end_i = data->row - 1;
    }

    // ������ �������� �� �������������� ������, ������� ���� �� ���� ��������, �� �� �� ���� �������
	for (int i = end_i - 1; i > start_i; i--) {
		for (int j = data->col - 2; j >= 1; j--) {

			float intensity = (float)(data->intensity[i][j]);

			values[0] = data->distance[i][j + 1] + a * intensity;
			values[1] = data->distance[i + 1][j] + a * intensity;
			values[2] = data->distance[i + 1][j + 1] + b * intensity;
			values[3] = data->distance[i + 1][j - 1] + b * intensity;
			values[4] = data->distance[i][j];

			float new_dist = get_min(values, num_comparison);
			if (new_dist != data->distance[i][j]) {

				data->distance[i][j] = new_dist;

				*solution_changed = 1;
			}
		}
	}

	free(values);

	return data->distance;
}


void print_solution(PGM_DATA* data) {

	for (int i = 0; i < data->row; i++) {
		for (int j = 0; j < data->col; j++) {
			printf("%f ", data->distance[i][j]);
		}
		printf("\n");
	}

	printf("\n\n\n");
}

float** calc_dist(PGM_DATA* data, int myid, int numprocs, int* iters_num) {

	int solution_changed = 1;
	int max_iter = 1000;
	int cur_iter = 0;

    // ���-�� ����� � ����� ������������ �����
    int block_size = data->row / numprocs;

    // ������� ������ � ��������� ����� ������������� �����
    int start_i = block_size * myid;
    int end_i = start_i + block_size - 1;

    // ���������� ������
    start_i -= 1;
    end_i += 1;

    // ������ ���� ��� ���������� ������ � - 1
    if (myid == 0){
        start_i = 0;
    }

    // ��� ���������� ����� ����� ������� ���������� ������, ���� ��� ��������
    if (myid + 1 == numprocs){
        end_i = data->row - 1;
    }

    // 0 �������� ������� ���
	while (cur_iter < max_iter) {

        // ��� ��������� �� ������ � ������� �������� �� ��������
		data->distance = calc_down(data, &solution_changed, myid, numprocs);
		data->distance = calc_up(data, &solution_changed, myid, numprocs);

        // ��� ��� ���������� �� ���� ������� ����� � 0 � ��� ������� ���� �� ������ ����. ��������
        if (myid == 0){
            int* procs_solution_changed = (int*) malloc (sizeof(int) * (numprocs - 1));

            for (int proc_num = 1; proc_num < numprocs; proc_num++){
                MPI_Recv( &(procs_solution_changed[proc_num - 1]), 1, MPI_INT, proc_num, ITER_STATUS, MPI_COMM_WORLD, &status );
            }

            // ��������� ����� ������ - 1, ���� � ����-�� ���-�� ����������
            for (int proc_num = 1; proc_num < numprocs; proc_num++){
                if (procs_solution_changed[proc_num - 1]){
                    solution_changed = 1;
                    break;
                }
            }
            for (int proc_num = 1; proc_num < numprocs; proc_num++){
                MPI_Send( &solution_changed, 1, MPI_INT, proc_num, ITER_STATUS, MPI_COMM_WORLD );
            }

        }
        else {
            // ���������� ���� ������� �� ������������ ���������
            MPI_Send( &solution_changed, 1, MPI_INT, 0, ITER_STATUS, MPI_COMM_WORLD );
            // �������� ����� ������ �� 0 ��������
            MPI_Recv( &solution_changed, 1, MPI_INT, 0, ITER_STATUS, MPI_COMM_WORLD, &status );
        }

        // ��� ��� ������ ���� �� ���� ������
		if (!solution_changed) {
            if (myid == 0){
                printf("Solution not changed on %d iteration", cur_iter);
            }
			break;
		}

        // ���� ������ �� ���������� -- ��� ���� �������� ���� ���������� 0 (��� ����� � run �������)

        // ���� ���������� �� ������ ������������ ���������� ����������

        // 0 �� ����������/�������� (��) ����. ��������, � ��������� �� ����������/�������� (��) ����������

        // ���� ��������, �� ������� ����������, ����� ���������
        if (myid % 2){

            // 0 �� ���������� ������ ������ ����������� ��������
            if (myid != 0){
                MPI_Send( &(data->distance[start_i + 1][0]), data->col, MPI_FLOAT, myid - 1, SEND_PREV, MPI_COMM_WORLD );
            }
            // ��������� �� ���������� ��������� ������ ���������� ��������
            if (myid + 1 != numprocs){
                MPI_Send( &(data->distance[end_i - 1][0]), data->col, MPI_FLOAT, myid + 1, SEND_NEXT, MPI_COMM_WORLD );
            }

            // ��� ����� ���������� �������� �������� ������ ����� ��������� ������ �� ����. ��������
            if (myid + 1 != numprocs){
                MPI_Recv( &(data->distance[end_i][0]), data->col, MPI_FLOAT, myid + 1, FROM_NEXT, MPI_COMM_WORLD, &status );
            }
            // ��� ����� 0 �������� �������� ������ 1 ������ �� ����. ��������
            if (myid != 0){
                MPI_Recv( &(data->distance[start_i][0]), data->col, MPI_FLOAT, myid - 1, FROM_PREV, MPI_COMM_WORLD, &status );
            }
        }
        else {
            // ��� ����� ���������� �������� �������� ������ ����� ��������� ������ �� ����. ��������
            if (myid + 1 != numprocs){
                MPI_Recv( &(data->distance[end_i][0]), data->col, MPI_FLOAT, myid + 1, FROM_NEXT, MPI_COMM_WORLD, &status );
            }
            // ��� ����� 0 �������� �������� ������ 1 ������ �� ����. ��������
            if (myid != 0){
                MPI_Recv( &(data->distance[start_i][0]), data->col, MPI_FLOAT, myid - 1, FROM_PREV, MPI_COMM_WORLD, &status );
            }

            // 0 �� ���������� ������ ������ ����������� ��������
            if (myid != 0){
                MPI_Send( &(data->distance[start_i + 1][0]), data->col, MPI_FLOAT, myid - 1, SEND_PREV, MPI_COMM_WORLD );
            }
            // ��������� �� ���������� ��������� ������ ���������� ��������
            if (myid + 1 != numprocs){
                MPI_Send( &(data->distance[end_i - 1][0]), data->col, MPI_FLOAT, myid + 1, SEND_NEXT, MPI_COMM_WORLD );
            }
        }


		solution_changed = 0;
		cur_iter += 1;

	}

	// ��� ������ ��������� � 0 �������
	if (myid == 0){
        for (int proc_num = 1; proc_num < numprocs - 1; proc_num++){

            MPI_Recv( &(data->distance[block_size * proc_num][0]), data->col * block_size, MPI_FLOAT, proc_num, FINAL_VER, MPI_COMM_WORLD, &status );
        }

        // � ���������� ����� ���� ������ ������, ������� ������ ������� ��������
        if (numprocs != 1){
            int last_proc_start_i = block_size * (numprocs - 1) - 1;
            int last_proc_end_i = data->row - 1;

            MPI_Recv( &(data->distance[last_proc_start_i][0]), data->col * (last_proc_end_i - last_proc_start_i - 1), MPI_FLOAT, numprocs - 1, FINAL_VER, MPI_COMM_WORLD, &status );
        }
	}
	else {
        MPI_Send( &(data->distance[start_i + 1][0]), data->col * (end_i - start_i - 1), MPI_FLOAT, 0, FINAL_VER, MPI_COMM_WORLD );
	}

    *iters_num = cur_iter;

	return data->distance;
}


int run(const char* filename, int myid, int numprocs) {
	srand(time(NULL));

    // ��� ������ �������� ��� - � ������� ����� ���� ����� �� ����
	PGM_DATA* data = (PGM_DATA*)malloc(sizeof(PGM_DATA));
	int iters_num = 0;

    if (myid == 0){
        // 0 ������� ���� ������ ����
        // printf("\nRead PGM\n");
        data = read_PGM(filename, data);

        // ����� 0 ���������� �� ������� ������ - row, col nums
        for (int rec_proc_num = 1; rec_proc_num < numprocs; rec_proc_num++){
            MPI_Send( &(data->col), 1, MPI_INT, rec_proc_num, COL_NUM, MPI_COMM_WORLD );
            MPI_Send( &(data->row), 1, MPI_INT, rec_proc_num, ROW_NUM, MPI_COMM_WORLD );
        }


        // 0 ������� �������� init ���������
        // printf("Init distance calc\n");
        data->distance = get_init_distance(data);

        for (int rec_proc_num = 1; rec_proc_num < numprocs; rec_proc_num++){
            MPI_Send( &(data->intensity[0][0]), data->row * data->col, MPI_INT, rec_proc_num, PGM_DAT, MPI_COMM_WORLD );
            MPI_Send( &(data->distance[0][0]), data->row * data->col, MPI_FLOAT, rec_proc_num, DIST_DAT, MPI_COMM_WORLD );
        }
    }
    else {
        // ����� ��������� ������� ������� ���� � ������� ������ rows cols �� 0 ��������
        MPI_Recv( &(data->col), 1, MPI_INT, 0, COL_NUM, MPI_COMM_WORLD, &status );
        MPI_Recv( &(data->row), 1, MPI_INT, 0, ROW_NUM, MPI_COMM_WORLD, &status );

        // �������� ������
        data->intensity = allocate_pgm_memory(data->row, data->col);
        data->distance = allocate_dist_memory(data->row, data->col);

        // ��� ������ �� 0 �������� �� �������������� �� ����� � init ����������
        MPI_Recv( &(data->intensity[0][0]), data->row * data->col, MPI_INT, 0, PGM_DAT, MPI_COMM_WORLD, &status );
        MPI_Recv( &(data->distance[0][0]), data->row * data->col, MPI_FLOAT, 0, DIST_DAT, MPI_COMM_WORLD, &status );
    }

	// ��� ������ � ���� ��� �����������-��������� (���. ������� - �����������) ���� ������

	// printf("Calc distance\n");

	// ���� ������ ��� ���, �� ������ ��� ������ �������� ����� ������� ������ 0 ��������
	data->distance = calc_dist(data, myid, numprocs, &iters_num);

	if (myid == 0){
        // 0 ������� ������ calc_dist ������ ������ �� ���� ���������
        write_result(data);
	}
    // ��� ������ ���
	free_pgm_memory(data->intensity, data->row);
	free_dist_memory(data->distance, data->row);

	return iters_num;
}

int main(int argc, char* argv[])
{
    int myid, numprocs;

    MPI_Init(&argc, &argv);        // starts MPI

    FILE *fpt;
    const char* filename = "data/pgmimg_1.pgm";
    const char* res_filename = "mpi_res.csv";
    int input_img_type = 0;  // 0 500x500; 1 - 50x5000; format - colsxrows
    int iters_num = 0;

    MPI_Comm_rank(MPI_COMM_WORLD, &myid);  // get current process id
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);      // get number of processeser

    clock_t begin = clock();
    iters_num = run(filename, myid, numprocs);

    clock_t end = clock();

    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

    if (myid == 0){

        printf("Process number %d time: %f\n", myid, time_spent);

        if (fpt = fopen(res_filename, "r"))
        {
            fclose(fpt);
            fpt = fopen(res_filename, "a");
            fprintf(fpt,"%d;%d;%f;%d\n", numprocs, iters_num, time_spent, input_img_type);

        }
        else {
            fpt = fopen(res_filename, "a");
            fprintf(fpt,"threads; iters; time; img_type\n");
            fprintf(fpt,"%d;%d;%f;%d\n", numprocs, iters_num, time_spent, input_img_type);
        }

        fclose(fpt);

    }
    MPI_Finalize();

    return 0;
}


