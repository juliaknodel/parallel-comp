# idx in array of element that stores key
cfg_line_key_idx = 0

# key value for parser
var_key = 'var'

# length of the array with data about variable
var_line_min_len = 4
# idx in array of element with data about variable name
var_name_idx = 1
# idx in array of element with data about variable value
var_val_idx = 3

# key that identifies line as data about fitness function
fit_func_key = 'fit_func'
# min length of the array that stores data about fitness function
fit_func_line_min_len = 2

# key that identifies line as comment
comment_key = '#'

# how much times fitness function can return None in recalculation cycle
MAX_CNT_COMP_ERROR = 10

# name of the file for storing optimization results
results_filename = 'results_cmd.csv'
