from params import cfg_line_key_idx, var_key, \
    var_name_idx, var_val_idx, \
    fit_func_key, \
    comment_key, fit_func_line_min_len, var_line_min_len, results_filename

from bee_colony_algo import Hive

import time
import os

from tqdm import tqdm
import pandas as pd


def parse_cfg(cfg_filename: str):
    """
    :param cfg_filename: config file name, includes var values and fit_func filename
    :return: var_dict: dictionary which includes all possible variables names with init values
    """

    # default params
    params = {'boundaries': None,
              'left_boundaries': None,
              'right_boundaries': None,
              'fit_func': None,
              'radius': 1,
              'n_best_food_sources': 10,
              'm_good_food_sources': 20,
              'k_other_food_sources': 30,
              'cnt_bees_on_best_food_sources': 10,
              'cnt_bees_on_good_food_sources': 5,
              'max_iter': 100,
              'max_iter_wo_improvement': 20,
              'n_parallel': os.cpu_count()}

    with open(cfg_filename) as cfg:
        for line in cfg:
            data = line.split()

            if line == "\n" or data[cfg_line_key_idx] == comment_key:
                pass
            # line with variable
            elif len(data) >= var_line_min_len and data[cfg_line_key_idx] == var_key:

                if data[var_name_idx] in params:

                    params[data[var_name_idx]] = \
                        int(data[var_val_idx]) if data[var_name_idx] not in ['left_boundaries',
                                                                             'right_boundaries'] else list(
                            float(val) for val in data[var_val_idx:])

                # var name not in dict of possible variables
                else:
                    print("Problem in cfg file, variable with name ", data[var_name_idx],
                          " doesn't exist")
                    return 0

            elif len(data) >= fit_func_line_min_len and data[cfg_line_key_idx] == fit_func_key:
                params['fit_func'] = " ".join(data[cfg_line_key_idx + 1:])

            # if not comment or empty line -> error
            else:
                print("Problem in cfg file, line '", line[: -1], "' is invalid")
                return 0

        if params['left_boundaries'] and params['right_boundaries'] and params['fit_func']:
            params['boundaries'] = list(zip(params['left_boundaries'], params['right_boundaries']))

            return params
        else:
            print('Mandatory params values missed in cfg file')
            return 0


def for_report():

    params = parse_cfg("D:/PycharmProjects/parallel-comp/lab1/conf.cfg")

    if not params:
        print('Problems with data in config file')
        return 0

    df = pd.DataFrame(columns=['id', 'type', 'pools', 'iter', 'solution', 'fit_func', 'time'], dtype=object)

    for i in tqdm(range(os.cpu_count())):

        for j in tqdm(range(5)):
            start_time = time.time()

            optimizer = Hive(boundaries=params['boundaries'],
                             fit_func=params['fit_func'],
                             radius=params['radius'],
                             n_best_food_sources=params['n_best_food_sources'],
                             m_good_food_sources=params['m_good_food_sources'],
                             k_other_food_sources=params['k_other_food_sources'],
                             cnt_bees_on_best_food_sources=params['cnt_bees_on_best_food_sources'],
                             cnt_bees_on_good_food_sources=params['cnt_bees_on_good_food_sources'],
                             max_iter=params['max_iter'],
                             max_iter_wo_improvement=params['max_iter_wo_improvement'],
                             n_parallel=i + 1)

            identifier = str(i) + ' ' + str(j)

            df = optimizer.optimize(identifier, df)
            df.to_csv(results_filename, sep=';')

            if not optimizer.bad_fit_func_flag:
                total_time = time.time() - start_time

                df = pd.concat([pd.DataFrame([[identifier, 'final', i + 1, optimizer.iter,
                                               optimizer.best_pos,
                                               optimizer.best_fitness, total_time]],
                                             columns=df.columns, dtype=object), df])

                df.to_csv(results_filename, sep=';')
            else:
                print('Comp error')


def main():

    # parse configuration file
    params = parse_cfg("D:/PycharmProjects/parallel-comp/lab1/conf.cfg")

    # if error returned from parser -> return
    if not params:
        print('Problems with data in config file')
        return 0

    # create DataFrame to store information for later analysis
    df = pd.DataFrame(columns=['id', 'type', 'pools', 'iter', 'solution', 'fit_func', 'time'], dtype=object)

    start_time = time.time()

    optimizer = Hive(boundaries=params['boundaries'],
                     fit_func=params['fit_func'],
                     radius=params['radius'],
                     n_best_food_sources=params['n_best_food_sources'],
                     m_good_food_sources=params['m_good_food_sources'],
                     k_other_food_sources=params['k_other_food_sources'],
                     cnt_bees_on_best_food_sources=params['cnt_bees_on_best_food_sources'],
                     cnt_bees_on_good_food_sources=params['cnt_bees_on_good_food_sources'],
                     max_iter=params['max_iter'],
                     max_iter_wo_improvement=params['max_iter_wo_improvement'],
                     n_parallel=params['n_parallel'])

    # default value bc in DataFrame will be only 1 row and no need in identifier
    identifier = '0'

    # start optimization
    df = optimizer.optimize(identifier, df)

    # write information about optimization (only for separate iterations) to csv
    df.to_csv(results_filename, sep=';')

    # if optimization stopped bc of fitness function returned None more than MAX_CNT_COMP_ERROR times
    if not optimizer.bad_fit_func_flag:
        total_time = time.time() - start_time

        # add information about optimization process (aggregated for all iterations)
        df = pd.concat([pd.DataFrame([[identifier, 'final', params['n_parallel'], optimizer.iter,
                                       optimizer.best_pos,
                                       optimizer.best_fitness, total_time]],
                                     columns=df.columns, dtype=object), df])

        # write DataFrame to .csv
        df.to_csv(results_filename, sep=';')
    else:
        print('Comp error')
