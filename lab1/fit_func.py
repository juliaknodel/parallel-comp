import sys
import numpy as np


def func(x):
    # return (1 - x[0]) ** 2 + 100 * ((x[1] - x[0] ** 2) ** 2)
    # return None
    A = 10
    n = 2
    return A*n + (x[0]**2 - A*np.cos(2*np.pi*x[0])) + (x[1]**2 - A*np.cos(2*np.pi*x[1]))


if __name__ == '__main__':
    float_args = [float(arg) for arg in sys.argv[1:]]
    print(func(float_args))
