import random
from multiprocessing.dummy import Pool as ThreadPool

from subprocess import check_call, STDOUT
from tempfile import NamedTemporaryFile

from params import MAX_CNT_COMP_ERROR, results_filename

from tqdm import tqdm
import pandas as pd
import time


def calc_with_cmd(fit_func: str, args: list):
    """
    calculate fitness_function(args) via shell
    :param fit_func: path to .py fitness function
    :param args: list with values of fitness function variables
    :return: fitness_function(args)
    """

    # takes 90sec for 10 iter
    # return check_output(['python', fit_func] + list(map(str, args)), shell=True).decode()

    # takes 33 sec for 10 iter
    with NamedTemporaryFile() as f:

        func_cmd = fit_func.split()
        check_call(func_cmd + list(map(str, args)), stdout=f, stderr=STDOUT)
        f.seek(0)

        res = f.read().decode()

        if res[0] == "N":
            return None
        else:
            return float(res)


class Bee:

    def __init__(self, boundaries: list, fit_func: str, radius):
        self.boundaries = boundaries
        self.fit_func = fit_func

        self.position = None
        self.set_random_position()

        self.fitness = 0

        self.radius = radius

    def set_position(self, position):
        self.position = [random.uniform(pos - self.radius, pos + self.radius) for pos in position]

        self.check_boundaries()

    def set_random_position(self):
        self.position = [random.uniform(self.boundaries[i][0],
                                        self.boundaries[i][1])
                         for i in range(len(self.boundaries))]

    def shift_with_cur_pos(self):
        self.set_position(self.position)

    def check_boundaries(self):
        self.position = [max(self.position[i], self.boundaries[i][0]) for i in range(len(self.position))]
        self.position = [min(self.position[i], self.boundaries[i][1]) for i in range(len(self.position))]

    def get_position(self):
        return self.position

    def calc_fitness(self):
        self.fitness = calc_with_cmd(self.fit_func, self.position)

    def get_fitness(self):
        return self.fitness

    def __lt__(self, other_bee):
        return self.fitness < other_bee.fitness


def calc_fitness_for_bee(bee):

    bee.calc_fitness()
    for _ in range(MAX_CNT_COMP_ERROR):
        if bee.fitness is None:
            bee.shift_with_cur_pos()
            bee.calc_fitness()
        else:
            return 0

    return 1


class Hive:

    def __init__(self,
                 boundaries: list,
                 fit_func: str,
                 radius: int,
                 n_best_food_sources: int,
                 m_good_food_sources: int,
                 k_other_food_sources: int,
                 cnt_bees_on_best_food_sources: int,
                 cnt_bees_on_good_food_sources: int,
                 max_iter: int,
                 max_iter_wo_improvement: int,
                 n_parallel: int):

        self.max_iter = max_iter
        self.iter = 0
        self.radius = radius
        self.max_iter_wo_improvement = max_iter_wo_improvement
        self.iter_wo_improvement = 0

        self.cnt_bees_best_fs = cnt_bees_on_best_food_sources
        self.cnt_bees_good_fs = cnt_bees_on_good_food_sources

        self.n_best_food_sources = n_best_food_sources
        self.m_good_food_sources = m_good_food_sources

        self.colony_size = \
            self.cnt_bees_best_fs * self.n_best_food_sources + \
            self.cnt_bees_good_fs * self.m_good_food_sources + \
            k_other_food_sources

        self.fit_func = fit_func
        self.boundaries = boundaries

        self.bees = [Bee(self.boundaries, self.fit_func, self.radius) for _ in range(self.colony_size)]

        self.n_parallel = n_parallel
        self.pool = ThreadPool(self.n_parallel)

        # flag to check if fitness_function returned None more than MAX_CNT_COMP_ERROR times
        self.bad_fit_func_flag = False

        res = self.pool.map(calc_fitness_for_bee, self.bees)

        if sum(res):
            self.bad_fit_func_flag = True
            return

        # sorting bees in ascending order of solution
        self.bees.sort()

        # best solution and fitness_function value for it on current iteration
        self.best_fitness = self.bees[0].get_fitness()
        self.best_pos = self.bees[0].get_position()

    def next_step(self):

        # best and good bees stay on their fs, get their positions
        # so fitness function cant get any worse in the next iteration
        best_fs = [bee.get_position() for bee in
                   self.bees[: self.n_best_food_sources]]

        good_fs = [bee.get_position() for bee in
                   self.bees[self.n_best_food_sources: self.n_best_food_sources + self.m_good_food_sources]]

        # get other bees and move part of them closer to the best and good positions
        other_bees = self.bees[self.n_best_food_sources + self.m_good_food_sources:]

        # send bees to the best fs
        for pos_num in range(len(best_fs)):
            for bee in other_bees[pos_num * (self.cnt_bees_best_fs - 1): (pos_num + 1) * (self.cnt_bees_best_fs - 1)]:
                bee.set_position(best_fs[pos_num])

        # use shift to continue moving bees that haven't been moved yet
        shift = self.n_best_food_sources * (self.cnt_bees_best_fs - 1)

        # send bees to the good fs
        for pos_num in range(len(good_fs)):
            for bee in other_bees[shift + pos_num * (self.cnt_bees_good_fs - 1): shift + (pos_num + 1) * (
                    self.cnt_bees_good_fs - 1)]:
                bee.set_position(good_fs[pos_num])

        # same with prev shift
        shift += self.m_good_food_sources * (self.cnt_bees_good_fs - 1)

        # send bees to the new random fs
        for bee in other_bees[shift:]:
            bee.set_random_position()

        res = self.pool.map(calc_fitness_for_bee, other_bees)

        # check if fitness_function returned None more than MAX_CNT_COMP_ERROR times
        if sum(res):
            self.bad_fit_func_flag = True
            return

        self.bees.sort()

        if self.best_fitness == self.bees[0].get_fitness():
            # if best solution did not change -> +1 iteration w/o improvement
            self.iter_wo_improvement += 1
        else:
            # if best solution becomes better -> reduce counter to 0
            self.iter_wo_improvement = 0

        self.best_fitness = self.bees[0].get_fitness()
        self.best_pos = self.bees[0].get_position()

        self.iter += 1

    def get_solution(self):
        return self.best_pos, self.best_fitness

    def optimize(self, identifier: str, df: pd.DataFrame):

        # add information about init values to DataFrame
        df = pd.concat([pd.DataFrame([[identifier, 'in process', self.n_parallel, self.iter,
                                       [self.best_pos],
                                       self.best_fitness, 0]],
                                     columns=df.columns, dtype=object), df])

        # write it to .csv
        df.to_csv(results_filename, sep=';')

        for _ in tqdm(range(self.max_iter)):

            # if solution didn't improve during max_iter_wo_improvement iterations -> stop
            if self.iter_wo_improvement == self.max_iter_wo_improvement:
                break

            start_time = time.time()

            # next iteration
            self.next_step()

            if self.bad_fit_func_flag:
                print('More than ', MAX_CNT_COMP_ERROR, ' times fitness function was returned with comp error')
                return df

            total_time = time.time() - start_time

            # write to DataFrame information about current iteration
            df = pd.concat([pd.DataFrame([[identifier, 'in process', self.n_parallel, self.iter,
                                           self.best_pos,
                                           self.best_fitness, total_time]],
                                         columns=df.columns, dtype=object), df])

        return df
